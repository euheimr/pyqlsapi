#pyqlsapi
A cloud-first Python implementation of the Qualys API V2 extended with AWS interoperability.
### WARNING: This is an *early* work in progress. Do not use this in any sort of production environment!!

### Requirements
1. Python>=3.8
2. lxml==4.5.2 - [main page](http://lxml.de/), 
    [pypi](https://pypi.org/project/lxml/), 
    [api docs](https://lxml.de/apidoc/lxml.html)
3. requests>=2.24.0 - [pypi](https://pypi.org/project/requests/), [api docs](https://requests.readthedocs.io/en/master/)
4. setuptools>=47.1.0 - []()
5. boto3

## Install
Simply clone the repository and extract. 

In that folder, create a new python file.
The library's functions can be called directly. 

_Not uploaded to pypi until release 1.0.0 is complete_

## Documentation
See the [/docs]() folder.


## Development
1. Clone the Repository and extract.

2. Install virtualenv globally:     
    
    ```pip install virtualenv```

3. Create the virtual environment from within the project folder:
    
    ```python -m venv venv```

4. Activate the virtual environment: 

    Windows: ``` venv\Scripts\activate```
    
    Linux: ``` source venv/Scripts/activate```

5. Install requirements:

    ```$ pip install -r requirements.txt```

[Optional] Generate documentation:

PyPi (pip)

    $ pip install -U sphinx
    
Debian/Ubuntu
    
    $ apt-get install python3-sphinx
        
RHEL, CentOS
        
    $ yum install python-sphinx



## Components (WIP):
_checkmark = done_
- [ ] Session (Connector) `qlsapi.session`
  - [ ] `qlsapi.session`
    - [x] `_check_token`
    - [x] `_build_query`
    - [ ] `query`
    - [x] `login`
    - [x] `logout`

- [ ] Vulnerability Management `qlsapi.vm`
  - [ ] `vm.Scans`
    - [ ] `list_scans`
    - [ ] `list_scheduled_scans`
    - [ ] `update_scheduled_scans`
    - [ ] `download_scan`
  
  - [ ] `vm.Reports`
    - [ ] `list_reports(api_call, scheduled=False)`
    - [ ] `list_scheduled_reports(api_call, scheduled=True)`
    - [ ] `download_report(api_call, path)`
    
- [ ] Policy Compliance `qlsapi.pc`
  - [ ] `pc.Scans`
    - [ ] `list_scans`
    - [ ] `download_scan`
    
  - [ ] `pc.Polcies`
    - [ ] `list_controls`
    - [ ] `list_policies`
    - [ ] `list_posture_info`
    
  - [ ] `pc.Exceptions`
    - [ ] `list_exceptions`
    - [ ] `request_exceptions`
    - [ ] `update_exceptions`
    
- [ ] Cloud Agent `qlsapi.ca`
  - [ ] `ca.Agents`
    - [ ] `list_agents`
    - [ ] `get_agent_count`
    - [ ] `activate_agent`
    - [ ] `deactivate_agent`
    - [ ] `deactivate_agent_multi`
    - [ ] `uninstall_agent`
    - [ ] `uninstall_agent_multi`
  - [ ] `ca.Activation`
    - [ ] `get_key`
    - [ ] `search_key`
    - [ ] `create_key` 
    
- [ ] Asset Management `qlsapi.am`
  - [ ] Authentication Records
    - [ ] `list_records`
    - [ ] `list_records_by_type`
    - [ ] `create_record`
    - [ ] `update_record`
    - [ ] `delete_record`
   
## Resources
To work on and help complete this project, I've referred to the following resources:
* [Qualys Documentation Page](https://www.qualys.com/documentation/)
    * [PDF] [Qualys API Quick Reference](https://www.qualys.com/docs/qualys-api-quick-reference.pdf)
    * [PDF] [Qualys API Limits](https://www.qualys.com/docs/qualys-api-limits.pdf)
    * [PDF] [Qualys API - Tracking API Usage](https://www.qualys.com/docs/qualys-api-tracking-api-usage.pdf)
    * [PDF] [Qualys API (VM,PC) User Guide](https://www.qualys.com/docs/qualys-api-vmpc-user-guide.pdf)
    * [PDF] [Qualys API (VM,PC) XML/DTD Reference](https://www.qualys.com/docs/qualys-api-vmpc-xml-dtd-reference.pdf)
    * [PDF] [Qualys API - Asset Management & Tagging v2 API](https://www.qualys.com/docs/qualys-asset-management-tagging-api-v2-user-guide.pdf)
    * [PDF] [Qualys API - Cloud Agent API User Guide](https://www.qualys.com/docs/qualys-ca-api-user-guide.pdf)
    * [PDF] [Qualys API - CloudView API User Guide](https://www.qualys.com/docs/qualys-cloudview-api-user-guide.pdf)
    * [PDF] [Web Application Scanning API - User Guide](https://www.qualys.com/docs/qualys-was-api-user-guide.pdf)
    * [PDF] [Web Application Firewall API - User Guide](https://www.qualys.com/docs/qualys-waf-api-user-guide.pdf)

* Books
    * [Python 2.7 & 3.4 Pocket Reference (O'Reilly)](https://www.amazon.com/gp/product/1449357016/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
    * [Regular Expression Pocket Reference: Regular Expressions For Perl, Ruby, Php, Python, C, Java And .Net (Pocket Reference (O'Reilly)](https://www.amazon.com/gp/product/0596514271/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
    * [Python for Data Analysis: Data Wrangling with Pandas, NumPy, and IPython](https://www.amazon.com/gp/product/1491957662/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
    * [Python for DevOps: Learn Ruthlessly Effective Automation](https://www.amazon.com/gp/product/149205769X/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
    * [High Performance Python: Practical Performant Programming for Humans Practical Performant Programming for Humans](https://www.amazon.com/gp/product/1449361595/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
    * [Fluent Python: Clear, Concise, and Effective Programming](https://www.amazon.com/gp/product/1491946008/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)

    * [Learn AWS Serverless Computing: A beginner's guide to using AWS Lambda, Amazon API Gateway, and services from Amazon Web Services](https://www.amazon.com/gp/product/1789958350/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
    * [AWS Security Cookbook: Practical solutions for managing security policies, monitoring, auditing, and compliance with AWS](https://www.amazon.com/gp/product/1838826254/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)

* GitHub
    * [Multiplex Plot](https://github.com/NicholasMamo/multiplex-plot)
        * Multiplex is a visualization library for Python built on these principles using [matplotlib](https://github.com/matplotlib/matplotlib/)
    * 

* Misc
    * [Links List] [Awesome Python](https://awesome-python.com/)
    * [Post Collection] [Python Awesome - Data Visualization](https://pythonawesome.com/tag/data-visualization/)    

## License
[BSD-3](https://github.com/euheimr/pyqlsapi/blob/master/LICENSE)