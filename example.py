#!/usr/bin/env python
# Python Version 3.8
# Created: 10/06/2020
__author__ = """Jacob Betz"""
__email__ = 'euhe@pm.me'
__version__ = '0.0.1'

from loguru import logger as _log
from pathlib import Path
#import json

from qlsapi import session
#from qlsapi import config as qlscfg
#from qlsapi import am
#from qlsapi import vmpc
#from qlsapi import ca
#from qlsapi import lib
#from qlsapi import log as qlslog

# Optionally choose where the config goes
config_name = 'qlss.json'
config_directory = Path.cwd().joinpath('\\' + config_name)

# Checks for an existing session. If a session token is found
#   in the json config file $HOME/qlss.json it returns True
#try:
#    _active_session = session.check_session(config_directory)

    #if _active_session:
        #config

# Creates a session token as a file 'qlss.json' in the user home folder
# session = Session.create_session()


# Qualys API base URL
hostname = session.hostname['us_3']
username = ''
password = ''

try:
    response = session.login(url=hostname,
                             username=username,
                             password=password,
                             timeout=10)
    if response:

        print(f'{str(response.text())}')
        print(f'Response Headers: {str(response.headers)}')
        print(f'Cookies: {str(response.cookies)}')
        print(f'Status Code: {str(response.status_code)}')
    #if session_key:


except Exception as ex:
    print(f'Exception: {str(ex)}')
